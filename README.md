# :wave: Hi! :rainbow:
My name is Kenny Johnston and I'm the VP of Product at Instabug. Here are some of my quick links.
* [LinkedIn](https://www.linkedin.com/in/kencjohnston/)
* [Twitter](https://twitter.com/kencjohnston)
* [GitLab](https://gitlab.com/kencjohnston)
* [Personal Feedback Form](https://forms.gle/uQgn9acSgFesBKWe7)

If we are working together, or even if we aren't, here are some things it might be helpful to know about me including those items marked with 👣 (the GitLab emojii for [iteration](https://about.gitlab.com/handbook/values/#iteration)) that I'm working on improving.

### General 
* I've worked in and around open-source for most of my technical career, I'm a firm believer in the [benefits of open code and open-society](https://cyber.harvard.edu/works/lessig/opensocd1.pdf) and the [importance of community](https://thenewstack.io/power-community-open-source/) in open-source projects.
* I had a previous career in political campaigns. While I [don't bring my political views to work](https://about.gitlab.com/handbook/values/#dont-bring-religion-or-politics-to-work), I do bring my belief that everyone desires to be a [valued member of a winning team on an inspiring mission](https://blog.rackspace.com/whats-core-rackspace-core-values). 
* My strengths tend towards the strategic, I work hard to focus on [getting things done](https://en.wikipedia.org/wiki/Getting_Things_Done) while continuously [improving my strategic thinking skills](https://hbr.org/2016/12/4-ways-to-improve-your-strategic-thinking-skills). 
* For a someone with strategic strengths I'm surprisingly task motivated - I enjoy breaking down tasks and checking off lists. I look at my [GitLab Activity](https://gitlab.com/kencjohnston) a bit too often.
* I'm demotivated by work without accomplishment. Meetings with conversation that doesn't end in something concrete frustrate me. Knowing I have a day full of non-action oriented meetings makes me want to hit the snooze button. 
* I believe in the power of repetition. Whether it is building customer empathy, creating a data driven organization, coaching team members, finding and attracting top-talent or communicating about change - we only build muscle and make our way towards mastery through regular practice. This leads me to create automation to regularly generate GitLab issues & tasks for myself and my teams.
* 👣 I often get bogged down in a search for efficiency, especially with new tasks. This can lead to my taking authority from DRIs. Feel free to call me out on it.
* I believe in [working smarter not harder](https://www.inc.com/john-rampton/work-smarter-not-harder-10-ways-to-be-more-effective-at-work.html), in an effort to [scale myself](https://firstround.com/review/our-6-must-reads-for-scaling-yourself-as-a-leader/). 
* I'm even-keeled. That can lead others to read into tone that wasn't there. I try to [prevent that from happening](https://www.fastcompany.com/3054178/5-ways-to-avoid-a-massive-email-misunderstanding).
* I've been known to come off as direct, and antagonistic in written communication. I can assure you I never intend to be antagonistic. I've worked to [conveying appropriate emotion](https://about.gitlab.com/company/culture/all-remote/informal-communication/#using-emojis-to-convey-emotion) (😃) in my direct, written communication. 
* I take my work seriously, but not myself. I think we could all use a little bit [more FUN in our work](https://www.entrepreneur.com/article/288223).
* I enjoy receiving public praise, but private praise fuels me. 
* I'm never the smartest person in the room, if I share opinions consider them [Minimally Viable](https://about.gitlab.com/handbook/values/#minimum-viable-change-mvc)
* I prefer to discuss things directly, both with my teams and my manager. I'd rather know someone's direct intention than to be coached around an issue socratically.
* I believe in [playing to your strengths](https://hbr.org/2005/01/how-to-play-to-your-strengths), and building complete teams based on coordinated strengths. 
* 👣 I aspire to use [inclusive language](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing), actively listen and eliminate [co-optation](https://www.youtube.com/watch?v=zNCrMEOqHpc). Please call out when I make mistakes or fail. I will ALWAYS thank you for it.
* 👣 I hope to inspire an upstanding (not bystanding), psychologically safe, and growth oriented work environment via my daily actions so that everyone can contribute in an environment where they experience belonging. 
* Outside of work I love to cook, play soccer, build puzzles and travel. I created this [5x5 presentation](https://docs.google.com/presentation/d/1K9Cpnr6gVJroLbbAbGaHMVPmpM5ICCZ1AESuo4N5u7A/edit?usp=sharing) to walk through more about me. 
* My dog [Petri](https://about.gitlab.com/company/team-pets/#87-petri) shows up frequently in Zoom calls. She's big, and can startle the un-initiated. Sometimes I'll leave my camera's view to let her outside. 

### Product
* I LOVE the function of Product Management. In my first tech-job after meeting a Product Manager I immediately thought - "That's who I want to be when I grow up!"
* I'm a strong believer that while product managers are typically hyper-efficient individuals, they are one of the few individuals driving a company's effectiveness.
* It can be hard to strike the balance between ambition through pursuit of new markets, and focus on delivering results in immediate adjacencies. Product Managers are one of the few groups looking out for those opportunities. I'm always interested in hearing and helping you evaluate new investment opportunities.
* One of the things that I consider a super-power of GitLab's is our empowerment of PMs as DRIs for prioritization. I view one of my jobs as a leader to be ensuring we avoid easy decisions that erode a PMs prioritization DRI-ness out of concern that "PMs shouldn't need to consider X" and instead empower PMs to make complete prioritization decisions. Sometimes short-term considerations over-ride that need, but we should always strive for a product system that empowers our product managers to include a global view in prioritization.
* 👣 I'm working on striking the right balance between providing more direct input into Product Direction and empowering autonomy in teams I support. Please do not hesitate to call me out if I'm being overly directive.
* 👣 As a Product Leader it's my job to ensure we are effectively using our R&D investment across my portfolio. This means I need to spend time spotting cross-stage initiatives, painting a vision for them and ensuring global context is provided to enable individual PMs to align around pursuing it. This can come in the form of Direction updates, Investment Cases or person and workflow research.
* Uniquely as a Product Leader, I find it critical to create global context and so go out of my way to share context broadley. This involves sharing global context (particularly business impact) learned from outside of my scoope within my scope AND, just as importantly, sharing context from within my scope outside of my scope.
* 👣 As a Product Leader I also serve as our foremost expert on our product and advocate for our product in GTM pursuits.  I try to involve myself in Strategic Pursuits where senior product leadership can help make an impact with prospects and customers. 
* I really enjoy coaching, mentoring and advising Product Managers. Do not hestitate to reach out to me.

### Leadership
* 👣 I'm a values leader, and I am very bought in to [GitLab's values](https://about.gitlab.com/handbook/values/). As a leader it is my responsibility to educate others, and project, promote and further our values. This includes identifying values that my team members individually identify closely with and [telling stories](https://www.arielgroup.com/values-based-storytelling/) about and promoting examples of exceptional value-based behavior. Please let me know if I'm failing to meet that standard. 
* I want to be the type of leader I'd like to have - Loving, Trusting, Fair and Consistent.
* 👣 My even keeled-ness can lead to my not showcasing my excitement. I'm working on making sure I pause, recognize accomplishment and highlight my excitement for where my team, product and company are headed.
* I aspire to be a servant leader, but I've got a long-way to go. Please provide me feedback to help me get there.
* I believe as a leader you get back what you give out. I once heard a talk that hypothesized that [Conway's Law](https://en.wikipedia.org/wiki/Conway%27s_law) tells us that if we want lovable results OUT of our teams, we have to have put love IN to them.
* I want each of my team members to know they are a valuable contributor to a winning team and give them an inspiring mission.
* Leaders need to keep a pulse on what is impacting team members - whether that is stable counterpart availability, life stress, or major corporate initiatives. I do this via an open line of direction communication, 1-1s and skip-levels. I will always thank you for making me aware of these impacts.
* I believe my role as a leader is to run straight at murkey or persistent problems. Feel free to point me in their direction. 
* I was changed by the book [Drive](https://www.amazon.com/Drive-Surprising-Truth-About-Motivates/dp/1594484805/r), it helped me understand motivation in a new way. I always want a [team of missionaries, not mercenaries](https://svpg.com/missionaries-vs-mercenaries/). 
* 👣 I'm working on being a better delegater, feel free to call me out if I'm leaving delegation ambigous (action, timeline, etc) or not delegating enough.
* I believe that the job of a leader is to create capacity, whether through structure, empowerment or inspiration. I do this by focusing a [big portion of my energy at my direct-reports](https://gitlab.com/kencjohnston/README/-/blob/master/Priorities.md#quarterly-priorities). 
* I tend to be direct, and I HATE micro-managing.

### Logistics
* I block off my calendar for family time/dinner/bed-time from 3:00PM - 7:50PM CT.
* My day typically starts early with a block of dedicated focus/work time from 6:30AM - 8:50AM CT.
* When you need to reach me, here are my communication priorities and preference:
    * GitLab issues - Direct mention me on issues, I prefer interacting with issues to all other collaboration mechanisms. I aggressively follow up on TODOs. 
    * Slack - If I'm working, I'm on slack. For small group discussions, never hesitate to create a dedicated public chat room. Please avoid DMs unless they really need to be private.
    * Email - I check email only periodically throughout the day. I have no alerts setup to inform me of newly arriving email. 
    * SMS - I have my phone by my side during regular work hours to review texts or receive calls. Outside of work hours I'm hit or miss on SMS.
    * Phone - If I'm in a meeting I will let my phone go to voicemail. I only return calls that leave voicemails. If it is urgent, call my phone twice back-to-back and I will step out of what I'm doing to answer.
* I'm a big believer in inbox zero and I process my GitLab TODOs, slack and email to zero multiple times a day.
* I avoid email for most everything (thanks GitLab!). Fun fact, I've had direct team members go years without sending me an email.

#### Mac Spaces Setup
* I use Mac Spaces and configure my desktop with four of them - I think of them with the following designations 
    * ACTIVE - This is a blank space where I move whatever my current focus is - nothing else is in that space than that activity. Usually VSCode is in this space as a result.
    * UP NEXT - I keep ToDoist, and a browser that has links to my Priorities, Issue Boards, Issue List, MR List and GitLab Activity. These are all listed in a folder called "UP NEXT" in my browser's bookmarks bar
    * INBOX - I keep Slack, Zoom and a browser that has tabs for my GitLab ToDos, GitLab Calendar and GitLab Gmail.
    * PERSONAL - I keep Spotify, and a browser (logged in with my personal gmail account) that has my personal Gmail, WhatsApp and Messages 

#### GitLab
* As mentioned above, GitLab issues are my preferred collaboration method. As a result, I'm an exclusive user of GitLab TODOs. Please @ mention me when responding to issues, otherwise I won't get the notification.
* After clearing GitLab TODOs, my primary focus is on my [personal GitLab issue board](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston) and [priorities](https://gitlab.com/kencjohnston/README/-/blob/master/Priorities.md).
* I review my personal issue board on a weekly basis while setting my priorities for the week.
* I use personally scoped (KCJ:: ) labels to track my personal status on those issues that are assigned to me.
* I have a [shortcut](https://gitlab.com/gitlab-com/Product/issues/new?nav_source=navbar) in my bookmark bar for creating a gitlab-com/product issue and use it to capture tasks for myself.  
* Because I believe in the power of repetition, I tend to create [regular process issue templates](https://gitlab.com/gitlab-com/Product/-/tree/main/.gitlab/issue_templates) to help me stick with that reptition. When doing so, I almost always create a "Retro Thread" for each instance of the issue so I invite continous improvement to any process.

#### Slack
* I'm an avid leaver of slack channels. I do this to reduce the noise that can come with Slack. I'm only members of channels that I want to be notified of ANY new content. If I'm not in a channel don't hestitate to invite me, and don't be offended when I leave after responding. If you are interested - here's how I've configued my Slack Settings to further avoid noise.
    * I only show "Unreads Only"
    * I sort via "Priority"
    * I use the "Compact" Message Theme
    * I use "Just display names"
    * I do NOT require a "Prompt to confirm" when everything is read
* I consider Slack to be asynchronous. If there is a timeline for a request you might have please specificy it. For example - "Hey, can you review the Kickoff list prior to the kickoff meeting tomorrow?"

#### ToDoist
* I use ToDoist for short-term tasks and reminders that aren't appropriate for GitLab issues.

### Teams I Support
I [support the Product Management team](https://about.gitlab.com/company/team/org-chart/) responsible for the Ops Section stages at GitLab. 
* I strive to provide [Situational Leadership](https://en.wikipedia.org/wiki/Situational_leadership_theory). As we work together I might ask you for your level of excitement and comfortability on a task. I do that to ascertain what type of leadership I should provide. Feel free to jump straight in and say - "Kenny I need telling/selling/participating/delegating support here."
* I strive to strike a balance in [multi-modal communication](https://about.gitlab.com/handbook/communication/#multimodal-communication) between bias for action and providing space for review and contribution. That can sometimes result in ignorance or annoyance resulting in low effectiveness. Please don't hesitate to call me out if I'm over or under communicating.
* One easy way to keep me abreast of your priorities is to send a note in the #product Slack channel once a week and @ mention me. 
* Follow the [GitLab handbook when communicating time-off](https://about.gitlab.com/handbook/paid-time-off/#communicating-your-time-off). No other notification or approval is required. It's my job as your manager to ensure you have adequate coverage while you take much needed (and deserved) breaks. Please create [Coverage Issues](https://about.gitlab.com/handbook/product/product-manager-role/#taking-time-off) for me to ensure you take a break without worrying about a mountain of catchup work.
* I follow GitLab's [Skip Level](https://about.gitlab.com/handbook/leadership/skip-levels/) 1:1 methodology within my org. During these times I will go out of my way to check in on team member well-being in an effort to help all the team members I support practice strong [rest ethic](https://about.gitlab.com/company/culture/all-remote/mental-health/#your-rest-ethic-is-as-important-as-your-work-ethic).
* I will never take offense to skip-leveling, if you have a concern [you can and should take it to whoever you think will be effective in alleviating it](https://about.gitlab.com/handbook/leadership/#communication-should-be-direct-not-hierarchical).
* Share what you learn, whether that is relevant articles, conference notes or TED talks.
* When sharing info with me, my preference is to consume YOUR expertise. Rather than sharing an Analyst Report relevant for your categories, share an MR where you reference it and highlight how it impacts your product scope.
* 👣 I don't always do the best just of communicating priority, and expected actions when requesting them from team members. I'm working on doing so to enable you to make smart effectiveness choices. I will always thank you for asking for the Why, or an indication of priority.
* I will add , `FYI`, `FYA`, `FYR` (and avoid the non-specific CC) to responses to ensure I'm setting expectations correctly.
   * FYI - For Your Information - No response or reaction required
   * FYA - For Your Action - A request to reassign all or a portion of work to you, this is where I tend to leave ambiguity. Feel free to respond very directly with a request for clarity. I will always thank you for doing so.
   * FYR - For Your Review - A request for review (sometimes utilizing the reviewer functionality) and approval

#### 1 to 5s
In order to get a better sense of our personal and professional ups and downs - I like to share "1 to 5s" at the start of 1:1s and team meetings. The process is for each team member to answer the question:
> As a combination both personal and professional - on a scale of 1-5 (with 1 being a horrible day and 5 being an amazing day) - how are you today?

### Personality Tests
* [Social Style](https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/) - Officially [Amiable but right down the middle](https://gitlab.com/kencjohnston/README/-/blob/master/social-styles.png)
* [Strengths Finder](https://www.gallup.com/cliftonstrengths/en/252137/home.aspx?utm_source=google&utm_medium=cpc&utm_campaign=Strengths_ECommerce_Brand_Search_US&utm_content=strengths%20finder&gclid=CjwKCAjwguzzBRBiEiwAgU0FT6jwCih5cTNJnta-h6m3YsJ5mAQG4dRikPdeCp79Wjp1mT0bGamTpRoCIM4QAvD_BwE) - Futuristic, Learner, Strategic, Ideation, Intellection (Note these are ALL in the Strategic Thinking quadrant)
* [StandOut](https://www.marcusbuckingham.com/the-gift-of-standout/) Strengths Assessment - My top two strength roles are [Pioneer and Stimulator](https://gitlab.com/kencjohnston/README/-/blob/master/KCJ-StandoutReport.pdf)
* [Myers-Briggs](https://en.wikipedia.org/wiki/Myers%E2%80%93Briggs_Type_Indicator) - [INFP](https://www.16personalities.com/infp-personality) - Borderline between I&E, strong N, border between T&F, extreme P

### Adaptable
I never shy from feedback, constructive criticism or suggested improvements. It happens so rarely in the corporate world, I will ALWAYS thank you for it. Please consider this document in that context. I'm accepting [issue submissions](https://gitlab.com/kencjohnston/README/issues) and [merge request](https://gitlab.com/kencjohnston/README/merge_requests).

You can also provide me direct (anonymous) feedback by filling out my [ongoing feedback form](https://forms.gle/uQgn9acSgFesBKWe7). 

I've marked the items I'm working on (mostly derived from direct feedback) with an iteration 👣. THANK YOU to all those who've helped me grow.
